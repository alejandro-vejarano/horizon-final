LogIn();

// --------------------------Dummies--------------------------
var nombreDummie1='Carlos Alejandro Parrado Salazar';
var correoDummie1='correo@gmail.com';
var passwordDummie1='12345';
var ComprobacionCorreo ='';
var ComprobacionPassword ='';

var nombreDummie2='';
var correoDummie2='';
var passwordDummie2='';

$("#btnLogIn").click(function(){
	ComprobacionLogIn();
});

$("#btnSignIn").click(function(){
	CrearUsuarioDummie();
});

function ComprobacionLogIn(){
	 ComprobacionCorreo = $("#txtCorreoLogIn").val();
	 ComprobacionPassword = $("#txtPasswordLogIn").val();
	
	if (ComprobacionCorreo == correoDummie1 && ComprobacionPassword==passwordDummie1){
		Swal.fire({
			icon: 'success',
			title: 'Bienvenido '+nombreDummie1,
			showConfirmButton: false,
			timer: 1500
		});
				$("#NombreDesplegable").append(nombreDummie1);
		home();
	}
	else{
		Swal.fire({
			icon: 'error',
			title: 'Revise los datos ingresados',
			showConfirmButton: false,
			timer: 1500
		});	}
}

function CrearUsuarioDummie(){

	 nombreDummie2=$("#txtNombreSignIn").val();
	 correoDummie2=$("#txtcorreoSignIn").val();
	 passwordDummie2=$("#txtcontrasenaSignIn").val();
	 $("#NombreDesplegable").append(nombreDummie2);
	 Swal.fire({
		icon: 'success',
		title: 'Bienvenido '+nombreDummie2,
		showConfirmButton: false,
		timer: 1500
	});
	 home();
}


// --------------------------Dummies--------------------------

var CalendarioCrear = 0;
$("#buttonLogin").click(function(){
	console.log("mprimeeee");
});


var contador = 1;
const btn = document.querySelector('#menu-btn');
const menu = document.querySelector('#sidemenu');



btn.addEventListener('click', e => {
	menu.classList.toggle("menu-expanded");
	menu.classList.toggle("menu-collapsed");

	document.querySelector('body').classList.toggle('body-expanded');
	
});

var activacion = 0;


function main(){
	$('.menu_bar').click(function(){
		// $('nav').toggle(); 
 
		if(contador == 1){
			$('nav').animate({
				left: '0'
			});
			contador = 0;
		} else {
			contador = 1;
			$('nav').animate({
				left: '-100%'
			});
		}
 
	});
 
};

function LogIn(){

	document.getElementById('LogIn').style.display="block";
	document.getElementById('SignIn').style.display="none";

	// --------------------------Menu Hamburguesa--------------------------
	document.getElementById('sidemenu').style.display="none";
	// --------------------------Menu Hamburguesa--------------------------

	// --------------------------Bibliotecas--------------------------
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	// --------------------------Bibliotecas--------------------------

	// --------------------------Usuarios--------------------------
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('CrearUsuarios').style.display="none";
	document.getElementById('ModificarUsuarios').style.display="none";
	document.getElementById('BuscarUsuarios').style.display="none";
	document.getElementById('EliminarUsuario').style.display="none";
	// --------------------------Usuarios--------------------------

	// --------------------------Materias--------------------------
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('CrearMateria').style.display="none";
	document.getElementById('ModificarMateria').style.display="none";
	document.getElementById('BuscarMateria').style.display="none";
	document.getElementById('EliminarMateria').style.display="none";
	// --------------------------Materias--------------------------

	// --------------------------LugaresDeEstudio--------------------------
	document.getElementById('menuLugaresDeEstudio').style.display="none";
	document.getElementById('CrearReserva').style.display="none";
	document.getElementById('ModificarReserva').style.display="none";
	document.getElementById('BuscarReserva').style.display="none";
	document.getElementById('EliminarReserva').style.display="none";
	// --------------------------LugaresDeEstudio--------------------------

	// --------------------------Tutorias--------------------------
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('ConfirmacionTutoria').style.display="none";
	document.getElementById('chatTutoria').style.display="none";
	activacion=0;
	// --------------------------Tutorias--------------------------
	document.getElementById('horariosTotal').style.display="none";
	document.getElementById('HorariosAgregar').style.display="none";
	document.getElementById('Tutorias').style.display="none";
	document.getElementById('TutoriasSolicitar').style.display="none";
	
	// --------------------------Habitos--------------------------
	document.getElementById('Habitos').style.display = "none";
	document.getElementById('portafolio1').style.display = "none";
}

function SignIn(){

	document.getElementById('LogIn').style.display="none";
	document.getElementById('SignIn').style.display="block";

	// --------------------------Menu Hamburguesa--------------------------
	document.getElementById('sidemenu').style.display="none";
	// --------------------------Menu Hamburguesa--------------------------

	// --------------------------Bibliotecas--------------------------
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	// --------------------------Bibliotecas--------------------------

	// --------------------------Usuarios--------------------------
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('CrearUsuarios').style.display="none";
	document.getElementById('ModificarUsuarios').style.display="none";
	document.getElementById('BuscarUsuarios').style.display="none";
	document.getElementById('EliminarUsuario').style.display="none";
	// --------------------------Usuarios--------------------------

	// --------------------------Materias--------------------------
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('CrearMateria').style.display="none";
	document.getElementById('ModificarMateria').style.display="none";
	document.getElementById('BuscarMateria').style.display="none";
	document.getElementById('EliminarMateria').style.display="none";
	// --------------------------Materias--------------------------

	// --------------------------LugaresDeEstudio--------------------------
	document.getElementById('menuLugaresDeEstudio').style.display="none";
	document.getElementById('CrearReserva').style.display="none";
	document.getElementById('ModificarReserva').style.display="none";
	document.getElementById('BuscarReserva').style.display="none";
	document.getElementById('EliminarReserva').style.display="none";
	// --------------------------LugaresDeEstudio--------------------------

	// --------------------------Tutorias--------------------------
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('ConfirmacionTutoria').style.display="none";
	document.getElementById('chatTutoria').style.display="none";
	document.getElementById('portafolio1').style.display = "none";
	activacion=0;
	// --------------------------Tutorias--------------------------
}

// ------------------------------Movimientos LogIn & Sign In-------------------
$("#LinkASignIn").click(function(){
	SignIn();
});

$("#LinkALogIn").click(function(){
	LogIn();
});
// ------------------------------Movimientos LogIn & Sign In-------------------

function home(){

	// --------------------------LogIn & SignIn--------------------------
	document.getElementById('sidemenu').style.display="block";
	document.getElementById('LogIn').style.display="none";
	document.getElementById('SignIn').style.display="none";
	// --------------------------LogIn & SignIn--------------------------

	// --------------------------Bibliotecas--------------------------
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	// --------------------------Bibliotecas--------------------------

	// --------------------------Usuarios--------------------------
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('CrearUsuarios').style.display="none";
	document.getElementById('ModificarUsuarios').style.display="none";
	document.getElementById('BuscarUsuarios').style.display="none";
	document.getElementById('EliminarUsuario').style.display="none";
	// --------------------------Usuarios--------------------------

	// --------------------------Materias--------------------------
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('CrearMateria').style.display="none";
	document.getElementById('ModificarMateria').style.display="none";
	document.getElementById('BuscarMateria').style.display="none";
	document.getElementById('EliminarMateria').style.display="none";
	// --------------------------Materias--------------------------

	// --------------------------LugaresDeEstudio--------------------------
	document.getElementById('menuLugaresDeEstudio').style.display="none";
	document.getElementById('CrearReserva').style.display="none";
	document.getElementById('ModificarReserva').style.display="none";
	document.getElementById('BuscarReserva').style.display="none";
	document.getElementById('EliminarReserva').style.display="none";
	// --------------------------LugaresDeEstudio--------------------------

	// --------------------------Tutorias--------------------------
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('ConfirmacionTutoria').style.display="none";
	document.getElementById('chatTutoria').style.display="none";
	// --------------------------Tutorias--------------------------

	// --------------------------Horarios--------------------------

	document.getElementById('horariosTotal').style.display="none";
	document.getElementById('HorariosAgregar').style.display="none";
	document.getElementById('Tutorias').style.display="none";
	document.getElementById('TutoriasSolicitar').style.display="none";
	
	// --------------------------Habitos--------------------------
	document.getElementById('Habitos').style.display = "none";
	document.getElementById('portafolio1').style.display = "block";
	document.getElementById('TituloHorizon').style.display = "none";
	// --------------------------Habitos--------------------------
	activacion = 0;

}

// ---------------------Bibliotecas----------------------

function mostrarMenuAdministracion(){
	document.getElementById('menuAdministracion').style.display="block";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('CrearUsuarios').style.display="none";
	document.getElementById('ModificarUsuarios').style.display="none";
	document.getElementById('BuscarUsuarios').style.display="none";
	document.getElementById('EliminarUsuario').style.display="none";
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('CrearMateria').style.display="none";
	document.getElementById('ModificarMateria').style.display="none";
	document.getElementById('BuscarMateria').style.display="none";
	document.getElementById('EliminarMateria').style.display="none";
	document.getElementById('menuLugaresDeEstudio').style.display="none";
	document.getElementById('CrearReserva').style.display="none";
	document.getElementById('ModificarReserva').style.display="none";
	document.getElementById('BuscarReserva').style.display="none";
	document.getElementById('EliminarReserva').style.display="none";
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('ConfirmacionTutoria').style.display="none";
	document.getElementById('chatTutoria').style.display="none";
	document.getElementById('ConfirmacionTutoria').style.display="none";
	document.getElementById('horariosTotal').style.display="none";
	document.getElementById('HorariosAgregar').style.display="none";
	document.getElementById('Tutorias').style.display="none";
	document.getElementById('TutoriasSolicitar').style.display="none";
	document.getElementById('Habitos').style.display = "none";
	document.getElementById('portafolio1').style.display = "none";
	document.getElementById('TituloHorizon').style.display = "block";
	activacion = 0;
}




function mostrarOpciones(){
	document.getElementById('MantenimientoBibliotecas').style.display="block";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	document.getElementById('menuAdministracion').style.display="none";
	activacion = 0;
}

function mostrarCrearBibliotecas(){
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="block";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	activacion = 0;
}

function mostrarModificarBibliotecas(){
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="block";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	activacion = 0;
}


function mostrarBuscarBibliotecas(){
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="block";
	document.getElementById('EliminarBiblioteca').style.display="none";
	activacion = 0;
}

function mostrarEliminarBibliotecas(){
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="block";	
	activacion = 0;
}


$("#btnAdministrarBibliotecas").click(function(){
	
	mostrarOpciones();
	
});

$("#btnRegresarAdministracion").click(function(){
	 mostrarMenuAdministracion();
});

$("#btnCrearBiblioteca").click(function(){
	 mostrarCrearBibliotecas();
});

$("#btnModificarBiblioteca").click(function(){
	 mostrarModificarBibliotecas();
});

$("#btnBuscarBiblioteca").click(function(){
	 mostrarBuscarBibliotecas();
});

$("#btnEliminarBiblioteca").click(function(){
	 mostrarEliminarBibliotecas();
});

$("#btnRegresarMenuBibliotecasCrear").click(function(){
	 mostrarOpciones();
});

$("#btnRegresarMenuBibliotecasModificar").click(function(){
	 mostrarOpciones();
});

$("#btnRegresarMenuBibliotecasBuscar").click(function(){
	 mostrarOpciones();
});

$("#btnRegresarMenuBibliotecasEliminar").click(function(){
	 mostrarOpciones();
});

$("#buttonLogin").click(function(){
	home();
});

$("#buttonAdmin").click(function(){
	mostrarMenuAdministracion();
});
// ---------------------Bibliotecas----------------------


// ---------------------Usuarios----------------------
function mostrarOpcionesUsuarios(){
	document.getElementById('MantenimientoUsuarios').style.display="block";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('CrearUsuarios').style.display="none";
	document.getElementById('ModificarUsuarios').style.display="none";
	document.getElementById('BuscarUsuarios').style.display="none";
	document.getElementById('EliminarUsuario').style.display="none";
	document.getElementById('Habitos').style.display = "none";
	activacion = 0;
}

function mostrarCrearUsuarios(){
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('CrearUsuarios').style.display="block";
}

function mostrarModificarUsuarios(){
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('ModificarUsuarios').style.display="block";
}

function mostrarBuscarUsuarios(){
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('BuscarUsuarios').style.display="block";
}

function mostrarEliminarUsuarios(){
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('EliminarUsuario').style.display="block";
}



$("#btnRegresarAdministracionUsuarios").click(function(){
	 mostrarMenuAdministracion();
});

$("#btnAdministrarUsuarios").click(function(){
	mostrarOpcionesUsuarios();
});

$("#btnCrearUsuario").click(function(){
	mostrarCrearUsuarios();
});

$("#btnModificarUsuario").click(function(){
	mostrarModificarUsuarios();
});

$("#btnBuscarUsuario").click(function(){
	mostrarBuscarUsuarios();
});

$("#btnEliminarUsuario").click(function(){
	mostrarEliminarUsuarios();
});

$("#btnRegresarMenuUsuariosCrear").click(function(){
	 mostrarOpcionesUsuarios();
});

$("#btnRegresarMenuUsuariosModificar").click(function(){
	 mostrarOpcionesUsuarios();
});

$("#btnRegresarMenuUsuariosBuscar").click(function(){
	 mostrarOpcionesUsuarios();
});

$("#btnRegresarMenuUsuariosEliminar").click(function(){
	 mostrarOpcionesUsuarios();
});
// ---------------------Usuarios----------------------


// ---------------------Materias----------------------

function mostrarOpcionesMaterias(){
	document.getElementById('MantenimientoMaterias').style.display="block";
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('CrearUsuarios').style.display="none";
	document.getElementById('ModificarUsuarios').style.display="none";
	document.getElementById('BuscarUsuarios').style.display="none";
	document.getElementById('EliminarUsuario').style.display="none";
	document.getElementById('CrearMateria').style.display="none";
	document.getElementById('ModificarMateria').style.display="none";
	document.getElementById('BuscarMateria').style.display="none";
	document.getElementById('EliminarMateria').style.display="none";
	document.getElementById('Habitos').style.display = "none";
	activacion = 0;
}

function mostrarCrearMaterias(){
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('CrearMateria').style.display="block";
	activacion = 0;
}

function mostrarModificarMaterias(){
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('ModificarMateria').style.display="block";
	activacion = 0;
}

function mostrarBuscarMaterias(){
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('BuscarMateria').style.display="block";
	activacion = 0;
}

function mostrarEliminarMaterias(){
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('EliminarMateria').style.display="block";
	activacion = 0;
}

$("#btnAdministrarMaterias").click(function(){
	 mostrarOpcionesMaterias();
});

$("#btnCrearMateria").click(function(){
	mostrarCrearMaterias();
});

$("#btnModificarMateria123").click(function(){
	mostrarModificarMaterias();
});

$("#btnBuscarMateria").click(function(){
	mostrarBuscarMaterias();
});

$("#btnEliminarMateria").click(function(){
	mostrarEliminarMaterias();
});


$("#btnRegresarMenuMateriasCrear").click(function(){
	 mostrarOpcionesMaterias();
});

$("#btnRegresarMenuMateriasModificar").click(function(){
	 mostrarOpcionesMaterias();
});

$("#btnRegresarMenuMateriasBuscar").click(function(){
	 mostrarOpcionesMaterias();
});

$("#btnRegresarMenuMateriasEliminar").click(function(){
	 mostrarOpcionesMaterias();
});

$("#btnRegresarAdministracionMaterias").click(function(){
	 mostrarMenuAdministracion();
});


// ---------------------Materias----------------------

// ---------------------LugaresDeEstudio----------------------
function mostrarMenuLugaresDeEstudio(){
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('CrearUsuarios').style.display="none";
	document.getElementById('ModificarUsuarios').style.display="none";
	document.getElementById('BuscarUsuarios').style.display="none";
	document.getElementById('EliminarUsuario').style.display="none";
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('CrearMateria').style.display="none";
	document.getElementById('ModificarMateria').style.display="none";
	document.getElementById('BuscarMateria').style.display="none";
	document.getElementById('EliminarMateria').style.display="none";
	document.getElementById('menuLugaresDeEstudio').style.display="block";
	document.getElementById('CrearReserva').style.display="none";
	document.getElementById('BuscarReserva').style.display="none";
	document.getElementById('ModificarReserva').style.display="none";
	document.getElementById('EliminarReserva').style.display="none";
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('ConfirmacionTutoria').style.display="none";
	document.getElementById('chatTutoria').style.display="none";
	document.getElementById('horariosTotal').style.display="none";
	document.getElementById('HorariosAgregar').style.display="none";
	document.getElementById('Tutorias').style.display="none";
	document.getElementById('TutoriasSolicitar').style.display="none";
	document.getElementById('Habitos').style.display = "none";
	document.getElementById('portafolio1').style.display = "none";
	document.getElementById('TituloHorizon').style.display = "block";
	activacion = 0;

}

function mostrarCrearReserva(){
	document.getElementById('menuLugaresDeEstudio').style.display="none";
	document.getElementById('CrearReserva').style.display="block";
	activacion = 0;
}

function mostrarModificarReserva(){
	document.getElementById('menuLugaresDeEstudio').style.display="none";
	document.getElementById('ModificarReserva').style.display="block";
	activacion = 0;
}

function mostrarBuscarReserva(){
	document.getElementById('menuLugaresDeEstudio').style.display="none";
	document.getElementById('BuscarReserva').style.display="block";
	activacion = 0;
}

function mostrarEliminarReserva(){
	document.getElementById('menuLugaresDeEstudio').style.display="none";
	document.getElementById('EliminarReserva').style.display="block";
	activacion = 0;
}

$("#buttonEspacios").click(function(){
	mostrarMenuLugaresDeEstudio();
});

$("#btnReservar").click(function(){
	mostrarCrearReserva();
});

$("#btnModificarReserva").click(function(){
	mostrarCrearReserva();
});

$("#btnBuscarReserva").click(function(){
	mostrarBuscarReserva();
});

$("#btnEliminarReservas").click(function(){
	mostrarEliminarReserva();
});


$("#btnRegresarMenuReservasCrear").click(function(){
	mostrarMenuLugaresDeEstudio();
});

$("#btnRegresarMenuReservasModificar").click(function(){
	mostrarMenuLugaresDeEstudio();
});

$("#btnRegresarMenuReservasBuscar").click(function(){
	mostrarMenuLugaresDeEstudio();
});

$("#btnRegresarMenuReservasEliminar").click(function(){
	mostrarMenuLugaresDeEstudio();
});



$("#btnInsertarReserva").click(function(){
	var date = new Date($('#fechacrearReserva').val());
	alert(date.toUTCString());
});
// ---------------------LugaresDeEstudio----------------------

// ---------------------Tutorias----------------------
function mostrarMenuTutorias(){
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('CrearUsuarios').style.display="none";
	document.getElementById('ModificarUsuarios').style.display="none";
	document.getElementById('BuscarUsuarios').style.display="none";
	document.getElementById('EliminarUsuario').style.display="none";
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('CrearMateria').style.display="none";
	document.getElementById('ModificarMateria').style.display="none";
	document.getElementById('BuscarMateria').style.display="none";
	document.getElementById('EliminarMateria').style.display="none";
	document.getElementById('menuLugaresDeEstudio').style.display="none";
	document.getElementById('CrearReserva').style.display="none";
	document.getElementById('BuscarReserva').style.display="none";
	document.getElementById('ModificarReserva').style.display="none";
	document.getElementById('EliminarReserva').style.display="none";
	document.getElementById('menuTutorias').style.display="block";
	document.getElementById('ConfirmacionTutoria').style.display="none";
	document.getElementById('chatTutoria').style.display="none";
	document.getElementById('horariosTotal').style.display="none";
	document.getElementById('HorariosAgregar').style.display="none";
	document.getElementById('Tutorias').style.display="block";
	document.getElementById('TutoriasSolicitar').style.display="none";
	document.getElementById('TutoriasModificar').style.display="none";
	document.getElementById('TutoriasCancelar').style.display="none";
	document.getElementById('TarjetasVisualizarTutorias').style.display="none";
	document.getElementById('Habitos').style.display = "none";
	document.getElementById('portafolio1').style.display = "none";
	document.getElementById('TituloHorizon').style.display = "block";
	activacion = 0;
}

function mostrarConfirmacionTutoria(){
	document.getElementById('ConfirmacionTutoria').style.display="block";
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('chatTutoria').style.display="none";
	activacion = 0;
}

function mostrarAsisistenciaTutoria(){
	document.getElementById('ConfirmacionTutoria').style.display="none";
	document.getElementById('chatTutoria').style.display="block";
	activacion=1;
	if (activacion==1) {

		var wsUri = "ws://25.59.104.225:30001";
				var websocket = new WebSocket(wsUri); //creamos el socket
				websocket.onopen = function(evt) { //manejamos los eventos...
					console.log("Conectado..."); //... y aparecerá en la pantalla
				};
				websocket.onmessage = function(evt) { // cuando se recibe un mensaje
					var generic="listo! recibido!";
					var result = JSON.parse(evt.data);
					var mensajerecib = result.message;
					console.log(mensajerecib);
					if(result.message===generic){
						Swal.fire('Mensaje Recibido');
					}else{
						$('#Recibidor').append("<br>"+ ">>" +result.message);
					}
					console.log("Mensaje recibido:" + result.message);
				};
				websocket.onerror = function(evt) {
					console.log("oho!.. error:" + evt.data);
					Swal.fire({
	  icon: 'error',
	  title: 'ERROR',
	  text: 'No se ha conectado al servidor',
		})
				};
	 
				function enviarMensaje(texto) {
	
					a={
						type:'message',
						message:texto,
						contentType: "JSON"
	
					};
					var mes=JSON.stringify(a);
					websocket.send(mes);
					console.log("Enviando:" + texto);
				}
	
	
				$("#boton").click(function(){
	
					if ($('#Envia').val() ===''){
						Swal.fire({
	  icon: 'error',
	  title: 'ERROR',
	  text: 'No puede enviar mensajes Vacios',
		})
	
					}
					else{
					var msg = $('#Envia').val();
					enviarMensaje(msg);
					$('#Envia').val(null);
	
					}
	
				});
	
	}
}

$("#btnRegresarTutoriaActiva").click(function(){
	mostrarConfirmacionTutoria();
});

$("#btnAsistirTutoria").click(function(){
	mostrarConfirmacionTutoria();
});

$("#btnRegresarMenuTutorias").click(function(){
	mostrarMenuTutorias();
});

$("#btnIngresarATutoria").click(function(){
	mostrarAsisistenciaTutoria();
});

$("#btnRegresarMenuTutorias").click(function(){
	mostrarMenuTutorias();
});
$("#btnRegresarMenuTutoriasSolicitar").click(function(){
	mostrarMenuTutorias();
});
$("#btnRegresarMenuTutoriasModificar").click(function(){
	mostrarMenuTutorias();
});
$("#btnRegresarMenuTutoriasCancelar").click(function(){
	mostrarMenuTutorias();
});
$("#btnRegresarMenuTutoriaVisualizar").click(function(){
	mostrarMenuTutorias();
});


$("#buttonTutorias").click(function(){
	mostrarMenuTutorias();
});
$("#btnPedirTutoria").click(function(){
	document.getElementById('TutoriasSolicitar').style.display="block";
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('TituloTutoria').style.display="none";
});	
$("#btnModificarTutoria").click(function(){
	document.getElementById('TutoriasModificar').style.display="block";
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('TituloTutoria').style.display="none";
});	
$("#btnCancelarTutoria").click(function(){
	document.getElementById('TutoriasCancelar').style.display="block";
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('TituloTutoria').style.display="none";
});
$("#btnBuscarTutoria").click(function(){
	document.getElementById('TarjetasVisualizarTutorias').style.display="block";
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('TituloTutoria').style.display="none";
});	
// ---------------------Tutorias----------------------

// ---------------------LugaresDeEstudio----------------------

// ---------------------Horarios----------------------
function mostrarMenuHorarios(){
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('CrearUsuarios').style.display="none";
	document.getElementById('ModificarUsuarios').style.display="none";
	document.getElementById('BuscarUsuarios').style.display="none";
	document.getElementById('EliminarUsuario').style.display="none";
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('CrearMateria').style.display="none";
	document.getElementById('ModificarMateria').style.display="none";
	document.getElementById('BuscarMateria').style.display="none";
	document.getElementById('EliminarMateria').style.display="none";
	document.getElementById('menuLugaresDeEstudio').style.display="none";
	document.getElementById('CrearReserva').style.display="none";
	document.getElementById('BuscarReserva').style.display="none";
	document.getElementById('ModificarReserva').style.display="none";
	document.getElementById('EliminarReserva').style.display="none";
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('ConfirmacionTutoria').style.display="none";
	document.getElementById('chatTutoria').style.display="none";
	document.getElementById('horariosTotal').style.display="block";
	document.getElementById('HorariosImport').style.display="block";
	document.getElementById('calendar').style.display="none";
	document.getElementById('buttonHorariosCrearAgregar').style.display="none";
	document.getElementById('buttonHorariosCrearAgregarAtras').style.display="none";
	document.getElementById('HorariosAgregar').style.display="none";
	document.getElementById('HorariosAgregarActualizar').style.display="none";
	document.getElementById('TituloAgenda').style.display="block";
	document.getElementById('HorariosAgendarBorrar').style.display="none";
	document.getElementById('TarjetasVisualizarHorarios').style.display="none";
	document.getElementById('Tutorias').style.display="none";
	document.getElementById('TutoriasSolicitar').style.display="none";
	document.getElementById('TutoriasSolicitar').style.display="none";
	document.getElementById('Habitos').style.display = "none";
	document.getElementById('portafolio1').style.display = "none";
	document.getElementById('TituloHorizon').style.display = "block";
	var activacion = 0;

}

$("#buttonHorarios").click(function(){
mostrarMenuHorarios();

});
$("#buttonHorariosCrear").click(function(){
	document.getElementById('calendar').style.display="block";
	document.getElementById('botonesHorarios').style.display="block";
	document.getElementById('HorariosImport').style.display="none";
	document.getElementById('buttonHorariosCrearAgregar').style.display="block";
	document.getElementById('buttonHorariosCrearAgregarAtras').style.display="block";
	
});
$("#buttonHorariosCrearAgregarAtras").click(function(){
	document.getElementById('HorariosImport').style.display="block";
	document.getElementById('calendar').style.display="none";
	document.getElementById('buttonHorariosCrearAgregar').style.display="none";
	document.getElementById('buttonHorariosCrearAgregarAtras').style.display="none";
});

class Calendar{
		constructor(id){
			this.cells = [];
			this.selectedDate = null;
			this.currentMonth = moment();
			this.elCalendar = document.getElementById(id);
			this.showTemplate();
			this.elGridBody = this.elCalendar.querySelector('.grid_body');
			this.elMonthName = this.elCalendar.querySelector('.month-name');  			
			this.showCells();
			
		}
		showTemplate(){
		this.elCalendar.innerHTML = this.getTemplate();
		this.addEventListenerToControls();
	}

	getTemplate(){
		let template = `
				<div class="calendar_header">
					<button type="button" class="control control-prev">&lt;</button>
					<span class="month-name">OCT 2020</span>
					<button type="button" class="control control--next">&gt;</button>
				</div>
				<div class="calendar_body">
					<div class="grid">
						<div class="grid_header">
							<span class="grid_cell grid_cell--gh">Lun</span>
							<span class="grid_cell grid_cell--gh">Mar</span>
							<span class="grid_cell grid_cell--gh">Mié</span>
							<span class="grid_cell grid_cell--gh">Jue</span>
							<span class="grid_cell grid_cell--gh">Vie</span>
							<span class="grid_cell grid_cell--gh">Sáb</span>
							<span class="grid_cell grid_cell--gh">Dom</span>
						</div>
						<div class="grid_body">
							
						</div>
					</div>	
				</div>
		`;
		return template; 
	}
	addEventListenerToControls(){
		let elControls = this.elCalendar.querySelectorAll('.control');
		elControls.forEach(elControl =>{
			elControl.addEventListener('click', e =>{
				let elTarget = e.target;
				if(elTarget.classList.contains('control--next')){
					this.changeMonth(true);
				}else{
					this.changeMonth(false);
				}
				this.showCells();
			});
		}); 
	}

	changeMonth(next = true){
		if (next) {
			this.currentMonth.add(1,'months');
		}else{
			this.currentMonth.subtract(1, 'months');
		}
	}


	showCells(){
		this.cells = this.generateDates(this.currentMonth);
		if (this.cells === null) {
			console.error('No fue posible generar las fechas del calendario.');
			return; 
		}
		this.elGridBody.innerHTML ='';
		let templateCells = ''; 
		let disabledClass='';
		for(let i = 0; i < this.cells.length; i++){
			disabledClass = '';
			if (!this.cells[i].isInCurrentMonth) {
				disabledClass = 'grid_cell--disabled';
			}
			templateCells += ` 	
				<span class="grid_cell grid_cell--gd ${disabledClass}" data-cell-id="${i}" >
					${this.cells[i].date.date()}
				</span>
			`;
		}
		this.elMonthName.innerHTML = this.currentMonth.format('MMM YYYY');
		this.elGridBody.innerHTML = templateCells;
		this.addEventListenerToCells();
	}

	generateDates(monthToShow = moment()){
		if(!moment.isMoment(monthToShow)){
			return null;
		}
		let dateStart = moment(monthToShow).startOf('month');
		let dateEnd = moment(monthToShow).endOf('month');
		let cells = [];
		while(dateStart.day() !== 1){
			dateStart.subtract(1, 'days');
		}

		while(dateEnd.day() !== 0){
			dateEnd.add(1, 'days');
		}

		do{
			cells.push({
				date: moment(dateStart),
				isInCurrentMonth: dateStart.month() === monthToShow.month()
			});
			dateStart.add(1, 'days');
		}while (dateStart.isSameOrBefore(dateEnd));
		return cells; 	
	}

	addEventListenerToCells(){
		let elCells = this.elCalendar.querySelectorAll('.grid_cell--gd');
		elCells.forEach( elCells =>{
			elCells.addEventListener('click', e=>{
				let elTarget = e.target;
				if (elTarget.classList.contains('grid_cell--disabled')) {
					return;
				}
				let selectedCell = this.elGridBody.querySelector('.grid_cell--selected');
				if(selectedCell){
					selectedCell.classList.remove('grid_cell--selected')
				}
				
				elTarget.classList.add('grid_cell--selected');
				this.selectedDate = this.cells[parseInt(elTarget.dataset.cellId)].date;
				this.elCalendar.dispatchEvent(new Event('change'));
				CalendarioCrear=this.cells[parseInt(elTarget.dataset.cellId)].date.format('LL');
				
			});

		});
	}
	getElement(){	

		return this.elCalendar;
	}
	value(){
		return this.selectedDate;
	}
}
$("#buttonHorariosCrearAgregar").click(function(){

	if(CalendarioCrear==0){
		alert("Debe seleccionar una fecha");
	}else{
		document.getElementById('HorariosImport').style.display="none";
		document.getElementById('calendar').style.display="none";
		document.getElementById('botonesHorarios').style.display="none";
		document.getElementById('HorariosAgregar').style.display="block";
		document.getElementById('TituloAgenda').style.display="none";
		
		console.log(CalendarioCrear);
	}
});
$("#buttonHorariosCrearAgregarAgendarCrear").click(function(){
	var datosAgregar1= document.getElementById("txtDescripcionAgregar").value;
	var datosAgregar2= document.getElementById("txtCrearAgregarID").value;
	
	if((datosAgregar1=="")||(datosAgregar2=="")){
		alert("Debe completar todos los espacios");
		
	}else{
		alert("Ha sido agregado");
	}
});	
$("#btnRegresarMenuAgendar").click(function(){
	document.getElementById('calendar').style.display="block";
	document.getElementById('botonesHorarios').style.display="block";
	document.getElementById('HorariosImport').style.display="none";
	document.getElementById('buttonHorariosCrearAgregar').style.display="block";
	document.getElementById('buttonHorariosCrearAgregarAtras').style.display="block";
	document.getElementById('HorariosAgregar').style.display="none";
});	

$("#buttonHorariosActualizar").click(function(){
	document.getElementById('HorariosAgregarActualizar').style.display="block";
	document.getElementById('HorariosImport').style.display="none";
	document.getElementById('TituloAgenda').style.display="none";
});
$("#btnRegresarMenuAgendarActualizar").click(function(){
	document.getElementById('HorariosImport').style.display="block";
	document.getElementById('HorariosAgregarActualizar').style.display="none";
	document.getElementById('TituloAgenda').style.display="block";
});	
$("#buttonHorariosActualizarAgendarCrear").click(function(){
	var datosActualizar1= document.getElementById("txtDescripcionAgendarActualizar").value;
	var datosActualizar2= document.getElementById("txtAgendarActualizarID").value;
	
	if((datosActualizar1=="")||(datosActualizar2=="")){
		alert("Debe completar todos los espacios");
		
	}else{
		alert("Ha sido Actualizados");
	}
});	
$("#buttonHorariosBorrar").click(function(){
	document.getElementById('HorariosImport').style.display="none";
	document.getElementById('TituloAgenda').style.display="none";
	document.getElementById('HorariosAgendarBorrar').style.display="block";
});

$("#buttonHorariosEliminarAgendar").click(function(){
	var datosBorrar1= document.getElementById("txtAgendarBorrarID").value;
	
	if((datosBorrar1=="")){
		alert("Debe completar todos los espacios");
		
	}else{
		alert("Ha sido Eliminado");
	}
});	
$("#btnRegresarMenuAgendarEliminar").click(function(){
	document.getElementById('HorariosImport').style.display="block";
	document.getElementById('HorariosAgendarBorrar').style.display="none";
	document.getElementById('TituloAgenda').style.display="block";
});
$("#buttonHorariosVisualizar").click(function(){
	document.getElementById('TarjetasVisualizarHorarios').style.display="block";
	document.getElementById('HorariosImport').style.display="none";
	document.getElementById('TituloAgenda').style.display="none";
});
$("#btnRegresarMenuAgendarVisualizar").click(function(){
	document.getElementById('HorariosImport').style.display="block";
	document.getElementById('TarjetasVisualizarHorarios').style.display="none";
	document.getElementById('TituloAgenda').style.display="block";
});

// ---------------------Habitos----------------------

function mostrarHabitos(){
	document.getElementById('menuAdministracion').style.display="none";
	document.getElementById('MantenimientoBibliotecas').style.display="none";
	document.getElementById('CrearBiblioteca').style.display="none";
	document.getElementById('ModificarBiblioteca').style.display="none";
	document.getElementById('BuscarBiblioteca').style.display="none";
	document.getElementById('EliminarBiblioteca').style.display="none";
	document.getElementById('MantenimientoUsuarios').style.display="none";
	document.getElementById('CrearUsuarios').style.display="none";
	document.getElementById('ModificarUsuarios').style.display="none";
	document.getElementById('BuscarUsuarios').style.display="none";
	document.getElementById('EliminarUsuario').style.display="none";
	document.getElementById('MantenimientoMaterias').style.display="none";
	document.getElementById('CrearMateria').style.display="none";
	document.getElementById('ModificarMateria').style.display="none";
	document.getElementById('BuscarMateria').style.display="none";
	document.getElementById('EliminarMateria').style.display="none";
	document.getElementById('menuLugaresDeEstudio').style.display="none";
	document.getElementById('CrearReserva').style.display="none";
	document.getElementById('BuscarReserva').style.display="none";
	document.getElementById('ModificarReserva').style.display="none";
	document.getElementById('EliminarReserva').style.display="none";
	document.getElementById('menuTutorias').style.display="none";
	document.getElementById('ConfirmacionTutoria').style.display="none";
	document.getElementById('chatTutoria').style.display="none";
	document.getElementById('horariosTotal').style.display="none";
	document.getElementById('HorariosImport').style.display="none";
	document.getElementById('calendar').style.display="none";
	document.getElementById('buttonHorariosCrearAgregar').style.display="none";
	document.getElementById('buttonHorariosCrearAgregarAtras').style.display="none";
	document.getElementById('HorariosAgregar').style.display="none";
	document.getElementById('HorariosAgregarActualizar').style.display="none";
	document.getElementById('TituloAgenda').style.display="none";
	document.getElementById('HorariosAgendarBorrar').style.display="none";
	document.getElementById('TarjetasVisualizarHorarios').style.display="none";
	document.getElementById('Tutorias').style.display="none";
	document.getElementById('TutoriasSolicitar').style.display="none";
	document.getElementById('TutoriasSolicitar').style.display="none";
	document.getElementById('Habitos').style.display = "block";
	document.getElementById('HeaderHabitos').style.display = "block";
	document.getElementById('horaeat').style.display = "none";
	document.getElementById('horasport').style.display = "none";
	document.getElementById('horasleep').style.display = "none";
	document.getElementById('resumen').style.display = "none";
	document.getElementById('portafolio1').style.display = "none";
	document.getElementById('TituloHorizon').style.display = "block";
	activacion = 0;
}

$("#buttonHabitos").click(function () {
	mostrarHabitos();
});

$(".suenio").click(function () {
	document.getElementById('HeaderHabitos').style.display = "none";
	document.getElementById('horasleep').style.display = "block";
	document.getElementById('horaeat').style.display = "none";
	document.getElementById('horasport').style.display = "none";	
	document.getElementById('resumen').style.display = "none";
});

$(".comer").click(function () {
	document.getElementById('HeaderHabitos').style.display = "none";
	document.getElementById('horaeat').style.display = "block";
	document.getElementById('horasleep').style.display = "none";
	document.getElementById('horasport').style.display = "none";
	document.getElementById('resumen').style.display = "none";
});

$(".deporte").click(function () {
	document.getElementById('HeaderHabitos').style.display = "none";
	document.getElementById('horasport').style.display = "block";
	document.getElementById('horaeat').style.display = "none";
	document.getElementById('horasleep').style.display = "none";
	document.getElementById('resumen').style.display = "none";
});

$("#btnEnviarSueño").click(function () {
	registroExitoso();
});

$("#btnEnviarComida").click(function () {
	registroExitoso();
});

$("#btnEnviarDeporte").click(function () {
	registroExitoso();
});

$("#btnRegresaHabitos1").click(function () {
	mostrarHabitos();
});

$("#btnRegresaHabitos2").click(function () {
	mostrarHabitos();
});

$("#btnRegresaHabitos3").click(function () {
	mostrarHabitos();
});

$("#btnRegresaHabitos4").click(function () {
	mostrarHabitos();
});



function registroExitoso(){
	Swal.fire({
		icon: 'success',
		title: 'Registro exitoso',
		showConfirmButton: false,
		timer: 1500
	});
}

$("#btnResumen").click(function () {
	document.getElementById('HeaderHabitos').style.display = "none";
	document.getElementById('resumen').style.display = "block";
	document.getElementById('horasleep').style.display = "none";
	document.getElementById('horaeat').style.display = "none";
	document.getElementById('horasport').style.display = "none";
});

/*Gráfico 1*/
var sleepHour = document.getElementById("sleepHour");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var frecuencia = {
  labels: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
  datasets: [{
    label: "Promedio de Horas de Sueño",
    data: [9, 6, 5, 4, 8, 7, 8],
  }]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 80,
      fontColor: 'black'
    }
  },
  title: {
	display: true,
	text: 'HORAS DE SUEÑO'
}
};

var lineChart = new Chart(sleepHour, {
  type: 'line',
  data: frecuencia,
  options: chartOptions
});
/*---- */

/*Gráfico 2*/
var canvaExcercise = document.getElementById("excerciseTime");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var exUsuario = {
  label: 'Su Tiempo Promedio Registrado',
  data: [25, 10, 35, 15, 35, 10, 10],
  backgroundColor: 'rgba(0, 99, 132, 0.6)',
  borderWidth: 0,
  yAxisID: "y-axis-density"
};

var exPromedio = {
  label: 'Tiempo Promedio en su Categoria',
  data: [15, 25, 35, 40, 35, 25, 25],
  backgroundColor: 'rgba(99, 132, 0, 0.6)',
  borderWidth: 0,
  yAxisID: "y-axis-gravity"
};

var exUnion = {
	labels: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
	datasets: [exUsuario, exPromedio]
};

var chartOptions = {
  scales: {
    xAxes: [{
      barPercentage: 1,
      categoryPercentage: 0.6
    }],
    yAxes: [{
      id: "y-axis-density"
    }, {
      id: "y-axis-gravity"
    }]
  },
  title: {
	display: true,
	text: 'EJERCICIO FÍSICO'
}
};

var barChart = new Chart(canvaExcercise, {
  type: 'bar',
  data: exUnion,
  options: chartOptions
});

/*---- */

/*Gráfico 3*/
var eatHour = document.getElementById("eatTime");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var eatDay = {
    labels: [
        "1",
        "2",
        "3",
        "4",
		"5",
		"6+"
    ],
    datasets: [
        {
            data: [0, 2, 1, 3, 0, 2],
            backgroundColor: [
                "#FF6384",
                "#F7D050",
                "#84FF63",
                "#8463FF",
				"#6384FF",
				"#FF5733"
            ]
        }]
};

var pieChart = new Chart(eatHour, {
  type: 'pie',
  data: eatDay,
    options: {
        title: {
            display: true,
            text: 'VECES QUE HA COMIDO EN EL DÍA'
        }
    }
});

/*---- */

// ---------------------Habitos----------------------